﻿using System.ComponentModel.DataAnnotations;

namespace VideoGame_Store_Backend.Models
{
    public class VideoGame
    {
        public int Id { get; set; }

        [Required]
        [MaxLength(100)]
        public string Name { get; set; }

        [MaxLength(100)]
        public string Developer { get; set; }

        [MaxLength(500)]
        public string Description { get; set; }

        [Required]
        [Url]
        public string ImageUrl { get; set; }

        [Required]
        public int Price { get; set; }

        [Required]
        public int Stock { get; set; }
    }
}
