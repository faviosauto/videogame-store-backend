using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Threading.Tasks;
using VideoGame_Store_Backend.Models.RequestParameters;

namespace VideoGame_Store_Backend.ModelBinder
{
    public class VideoGameQueryModelBinder : IModelBinder
    {
        public Task BindModelAsync(ModelBindingContext bindingContext)
        {
            if (bindingContext == null)
            {
                throw new ArgumentNullException(nameof(bindingContext));
            }

            var search = bindingContext.ValueProvider.GetValue("search");

            if (search.Length == 0)
            {
                return Task.CompletedTask;
            }

            var searchData = search.FirstValue.Split(new char[] { '|' });

            if (searchData.Length >= 1)
            {
                var result = new VideogameRequestParameters
                {
                    Search = searchData[0]
                };

                bindingContext.Result = ModelBindingResult.Success(result);
            }

            return Task.CompletedTask;
        }
    }
}

