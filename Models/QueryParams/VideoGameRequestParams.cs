using Microsoft.AspNetCore.Mvc;
using VideoGame_Store_Backend.ModelBinder;

namespace VideoGame_Store_Backend.Models.RequestParameters
{
    public class VideogameRequestParameters
    {
        public string Search { get; set; }
    }
}