using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json.Serialization;
using VideoGame_Store_Backend.DataAccess.Context;
using VideoGame_Store_Backend.DataAccess.Repositories;
using VideoGame_Store_Backend.DataAccess.Repositories.Interfaces;
using VideoGame_Store_Backend.Models;
using VideoGame_Store_Backend.Services;
using VideoGame_Store_Backend.Services.Interfaces;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using System.Web.Http;

namespace VideoGame_Store_Backend
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy("ExcelDownload", builder =>
                {
                    builder.WithOrigins("*").AllowAnyHeader();
                });
            });

            services.AddControllersWithViews().AddNewtonsoftJson(options => options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore);
            services.AddDbContext<VideoGameStoreContext>(opt => opt.UseSqlServer(Configuration.GetConnectionString("VideogameConnection")));
            services.AddControllers().AddNewtonsoftJson(s =>
            {
                s.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            });
            services.AddScoped<IRepository<VideoGame>, VideoGameRepository>();
            services.AddScoped<IVideoGameService, VideoGameService>();
            services.AddScoped<IRepository<Order>, OrderRepository>();
            services.AddScoped<IOrderService, OrderService>();
            services.AddScoped<IRepository<OrderDetail>, OrderDetailRepository>();
            services.AddScoped<IOrderDetailService, OrderDetailsService>();
            services.AddScoped<DbContext, VideoGameStoreContext>();
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(opt =>
                {
                    opt.Audience = Configuration["AAD:ResourceId"];
                    opt.Authority = $"{Configuration["AAD:InstanceId"]}{Configuration["AAD: TenantId"]}";
                });

            System.Text.Encoding.RegisterProvider(System.Text.CodePagesEncodingProvider.Instance);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthentication();

            app.UseCors();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }

        
        public static void ConfigureFormatters(HttpConfiguration configuration)
        {
            configuration.Formatters.Remove(configuration.Formatters.XmlFormatter);
            configuration.Formatters.Remove(configuration.Formatters.JsonFormatter);
        }
    }
}
