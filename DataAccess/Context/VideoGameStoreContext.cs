﻿using VideoGame_Store_Backend.Models;
using Microsoft.EntityFrameworkCore;

namespace VideoGame_Store_Backend.DataAccess.Context
{
    public class VideoGameStoreContext : DbContext
    {
        public VideoGameStoreContext(DbContextOptions<VideoGameStoreContext> options) : base(options)
        {
        }

        public DbSet<VideoGame> Videogames { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderDetail> OrderDetails { get; set; }
        public DbSet<User> Users { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Seed();
        }
    }
}
