﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VideoGame_Store_Backend.Models
{
    public static class ModelBuilderExtensions
    {
        public static void Seed(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<VideoGame>().HasData(
                    new VideoGame
                    {
                        Id = 1,
                        Name = "The Legend of Zelda: Breathe of the Wild",
                        Developer = "Nintendo",
                        Description = "The Legend of Zelda: Breath of the Wild is a 2017 action-adventure game developed and published by Nintendo for the Nintendo Switch and Wii U consoles. Breath of the Wild is part of the Legend of Zelda franchise and is set at the end of the series' timeline; the player controls Link, who awakens from a hundred-year slumber to defeat Calamity Ganon before it can destroy the kingdom of Hyrule.",
                        ImageUrl = "https://images.unsplash.com/photo-1585857188823-77658a70979a?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1951&q=80",
                        Stock = 25,
                        Price = 350
                    },
                    new VideoGame
                    {
                        Id = 2,
                        Name = "God of War",
                        Developer = "Santa Monica Studios",
                        Description = "God of War is an action-adventure game developed by Santa Monica Studio and published by Sony Interactive Entertainment (SIE). Released on April 20, 2018, for the PlayStation 4 (PS4), it is the eighth installment in the God of War series, the eighth chronologically, and the sequel to 2010's God of War III. Unlike previous games, which were loosely based on Greek mythology, this installment is rooted in Norse mythology, with the majority of it set in ancient Norway in the realm of Midgard.",
                        ImageUrl = "https://images.unsplash.com/photo-1585857188823-77658a70979a?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1951&q=80",
                        Stock = 7,
                        Price = 160
                    },
                    new VideoGame
                    {
                        Id = 3,
                        Name = "Sekiro: Shadows Die Twice",
                        Developer = "FromSoftware",
                        Description = "Sekiro: Shadows Die Twice is an action-adventure video game developed by FromSoftware and published by Activision. The game follows a shinobi known as Wolf as he attempts to take revenge on a samurai clan who attacked him and kidnapped his lord. It was released for Microsoft Windows, PlayStation 4, and Xbox One on 22 March 2019.",
                        ImageUrl = "https://images.unsplash.com/photo-1585857188823-77658a70979a?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1951&q=80",
                        Stock = 15,
                        Price = 420
                    }
                );
        }
    }
}
