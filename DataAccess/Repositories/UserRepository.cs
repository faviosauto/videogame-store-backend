using Microsoft.AspNetCore.JsonPatch;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using VideoGame_Store_Backend.DataAccess.Context;
using VideoGame_Store_Backend.DataAccess.Repositories.Interfaces;
using VideoGame_Store_Backend.Models;

namespace VideoGame_Store_Backend.DataAccess.Repositories
{
    public class UserRepository : IRepository<User>
    {
        private VideoGameStoreContext _context;

        public UserRepository(VideoGameStoreContext context)
        {
            _context = context;
        }

        public IQueryable<User> GetAll()
        {
            return _context.Users;
        }

        public User GetById(int id)
        {
            return _context.Users.FirstOrDefault(user => user.Id == id);
        }

        public void Add(User user)
        {
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }

            _context.Users.Add(user);
            _context.SaveChanges();
        }

        public void Update(int id, object patch)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public void Update(int id, JsonPatchDocument patch)
        {
            throw new NotImplementedException();
        }
    }
}
