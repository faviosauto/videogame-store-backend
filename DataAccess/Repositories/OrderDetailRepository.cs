using Microsoft.AspNetCore.JsonPatch;
using System;
using System.Collections.Generic;
using System.Linq;
using VideoGame_Store_Backend.DataAccess.Context;
using VideoGame_Store_Backend.DataAccess.Repositories.Interfaces;
using VideoGame_Store_Backend.Models;

namespace VideoGame_Store_Backend.DataAccess.Repositories
{
    public class OrderDetailRepository : IRepository<OrderDetail>
    {
        private VideoGameStoreContext _context;

        public OrderDetailRepository(VideoGameStoreContext context)
        {
            _context = context;
        }

        public IQueryable<OrderDetail> GetAll()
        {
            IQueryable<OrderDetail> details = _context.OrderDetails;

            return details;
        }

        public OrderDetail GetById(int id)
        {
            return _context.OrderDetails.FirstOrDefault(orderDetail => orderDetail.Id == id);
        }

        public void Add(OrderDetail orderDetail)
        {
            if (orderDetail == null)
            {
                throw new ArgumentNullException(nameof(orderDetail));
            }
           

            _context.OrderDetails.Add(orderDetail);
        }

        public void Update(int id, object patch)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public void Update(int id, JsonPatchDocument patch)
        {
            throw new NotImplementedException();
        }
    }
}
