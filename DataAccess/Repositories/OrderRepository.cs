using Microsoft.AspNetCore.JsonPatch;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using VideoGame_Store_Backend.DataAccess.Context;
using VideoGame_Store_Backend.DataAccess.Repositories.Interfaces;
using VideoGame_Store_Backend.Models;

namespace VideoGame_Store_Backend.DataAccess.Repositories
{
    public class OrderRepository : IRepository<Order>
    {
        private VideoGameStoreContext _context;

        public OrderRepository(VideoGameStoreContext context)
        {
            _context = context;
        }

        public IQueryable<Order> GetAll()
        {
            return _context.Orders.Include(Order => Order.OrderDetails);
        }

        public Order GetById(int id)
        {
            return _context.Orders.FirstOrDefault(order => order.Id == id);
        }

        public void Add(Order Order)
        {
            if (Order == null)
            {
                throw new ArgumentNullException(nameof(Order));
            }

            foreach (var orderDetail in Order.OrderDetails)
            {
                if (orderDetail.VideoGame?.Id > 0)
                {
                    var videogame = _context.Videogames.Find(orderDetail.VideoGame.Id);

                    if (videogame != null)
                    {
                        orderDetail.VideoGame = videogame;
                    }
                }
            }

            _context.Orders.Add(Order);
            _context.SaveChanges();
        }

        public void Update(int id, object patch)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public void Update(int id, JsonPatchDocument patch)
        {
            throw new NotImplementedException();
        }
    }
}
