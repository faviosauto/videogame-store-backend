using Microsoft.AspNetCore.JsonPatch;
using System;
using System.Linq;
using VideoGame_Store_Backend.DataAccess.Context;
using VideoGame_Store_Backend.DataAccess.Repositories.Interfaces;
using VideoGame_Store_Backend.Models;

namespace VideoGame_Store_Backend.DataAccess.Repositories
{
    public class VideoGameRepository : IRepository<VideoGame>
    {
        private VideoGameStoreContext _context;

        public VideoGameRepository(VideoGameStoreContext context)
        {
            _context = context;
        }

        public IQueryable<VideoGame> GetAll()
        {
            return _context.Videogames;
        }

        public VideoGame GetById(int id)
        {
            return _context.Videogames.FirstOrDefault(p => p.Id == id);
        }

        public void Add(VideoGame videoGame)
        {
            if (videoGame == null)
            {
                throw new ArgumentNullException(nameof(videoGame));
            }

            _context.Videogames.Add(videoGame);
            _context.SaveChanges();
        }

        public void Update(int id, JsonPatchDocument patch)
        {
            var videogame = _context.Videogames.FirstOrDefault(p => p.Id == id);

            patch.ApplyTo(videogame);
            _context.SaveChanges();
        }

        public void Delete(int id)
        {
            var videogame = _context.Videogames.FirstOrDefault(p => p.Id == id);
            _context.Remove(videogame);
            _context.SaveChanges();
        }

        public void Update(int id, object patch)
        {
            throw new NotImplementedException();
        }
    }
}
