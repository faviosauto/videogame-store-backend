﻿using Microsoft.AspNetCore.JsonPatch;
using System.Linq;

namespace VideoGame_Store_Backend.DataAccess.Repositories.Interfaces
{
    public interface IRepository<T>
    {
        IQueryable<T> GetAll();

        T GetById(int id);

        void Add(T t);

        void Update(int id, JsonPatchDocument patch);

        void Delete(int id);
    }
}
