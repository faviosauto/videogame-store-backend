using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.IO;
using VideoGame_Store_Backend.Models;
using VideoGame_Store_Backend.Services.Interfaces;

namespace VideoGame_Store_Backend.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    [EnableCors("ExcelDownload")]
    public class OrdersController : ControllerBase
    {
        private readonly IOrderService _orderService;

        public OrdersController(IOrderService orderService)
        {
            _orderService = orderService;
        }

        [HttpGet]
        public ActionResult<ICollection<Order>> GetOrders()
        {
            var orders = _orderService.ListOrders();

            return Ok(orders);
        }


        [HttpGet("{id}")]
        public ActionResult<Order> GetOrderById(int id)
        {
            var order = _orderService.GetOrderById(id);

            if (order == null)
            {
                return NotFound();
            }

            return Ok(order.OrderDetails);
        }

        [HttpPost]
        public ActionResult<Order> CreateOrder(Order order)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            var orderCreated = _orderService.CreateOrder(order);

            return Ok(order);
        }

        [HttpGet("download")]
        
        public ActionResult DownloadExcel()
        {
            MemoryStream memoryStream = _orderService.DownloadExcel();

            return Ok(memoryStream);
        }
    }
}