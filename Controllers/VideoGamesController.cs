﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using NLog;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using VideoGame_Store_Backend.ModelBinder;
using VideoGame_Store_Backend.Models;
using VideoGame_Store_Backend.Models.RequestParameters;
using VideoGame_Store_Backend.Services.Interfaces;

namespace VideoGame_Store_Backend.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    [EnableCors("ExcelDownload")]
    public class VideoGamesController : ControllerBase
    {
        private readonly IVideoGameService _videoGameService;
        private readonly IOrderDetailService _orderDetailService;

        public VideoGamesController(IVideoGameService videoGameService, IOrderDetailService orderDetailService)
        {
            _videoGameService = videoGameService;
            _orderDetailService = orderDetailService;
        }

        // GET api/videogames
        [HttpGet]
        public ActionResult<ICollection<VideoGame>> GetVideoGames([ModelBinder(BinderType = typeof(VideoGameQueryModelBinder))] VideogameRequestParameters query)
        {
            var videogames = _videoGameService.ListVideoGames(query);

            return Ok(videogames);
        }

        // GET api/videogames/:id
        [HttpGet("{id}")]
        public ActionResult<VideoGame> GetVideoGameById(int id)
        {
            var videogame = _videoGameService.GetVideoGameById(id);

            if (videogame == null)
            {
                return NotFound();
            }

            return Ok(videogame);
        }

        // POST api/videogames
        [HttpPost]
        public ActionResult<VideoGame> CreateVideoGame(VideoGame videoGame)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            Logger log = LogManager.GetCurrentClassLogger();

            var videogame = _videoGameService.CreateVideoGame(videoGame);

            log.Info("Videogames has been succesfully added.\nVideogame ID: {VideogameInfo}", videogame.Id);

            return Ok(videogame);
        }


        [HttpPatch("{id}")]
        [Authorize]
        public IActionResult PatchVideogame(int id, [FromBody] JsonPatchDocument patch)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            _videoGameService.PatchVideoGame(id, patch);

            return Ok();
        }

        [HttpDelete("{id}")]
        [Authorize]
        public IActionResult DeleteVideogame(int id)
        {
            ICollection<OrderDetail> Orders = _orderDetailService.ListOrderDetails();
            IEnumerable<OrderDetail> videogameInOrder =
                from order in Orders where order.VideoGame.Id == id select order;

            var result = Orders.Where(v => v.Id == id).Any();

            if (result)
            {
                Logger log = LogManager.GetCurrentClassLogger();
                log.Error("Someone is trying to delete a videogame already in an order.");

                return BadRequest();
            }

            _videoGameService.DeleteVideoGame(id);

            return Ok();
        }

        // Download Excel with Videogames
        [HttpGet("download")]
        [EnableCors("ExcelDownload")]
        [Authorize]
        public ActionResult DownloadExcel()
        {
            MemoryStream memoryStream = _videoGameService.DownloadExcel();

            return Ok(memoryStream);
        }

        [HttpPost("upload")]
        [EnableCors]
        public ActionResult UploadExcel()
        {
            _videoGameService.UploadExcel();

            return Ok();
        }
    }
}