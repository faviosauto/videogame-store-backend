using ExcelExportPackage;
using Microsoft.AspNetCore.JsonPatch;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using VideoGame_Store_Backend.DataAccess.Repositories;
using VideoGame_Store_Backend.DataAccess.Repositories.Interfaces;
using VideoGame_Store_Backend.Models;
using VideoGame_Store_Backend.Models.RequestParameters;
using VideoGame_Store_Backend.Services.Interfaces;

namespace VideoGame_Store_Backend.Services
{
    public class VideoGameService : IVideoGameService
    {
        private readonly IRepository<VideoGame> _repository;

        public VideoGameService(IRepository<VideoGame> repository)
        {
            _repository = repository;
        }

        public VideoGame CreateVideoGame(VideoGame videogame)
        {
            _repository.Add(videogame);

            return videogame;

        }

        public void DeleteVideoGame(int id)
        {
            var videogame = _repository.GetById(id);


            if (videogame != null)
            {

                _repository.Delete(id);
            }
        }

        public VideoGame GetVideoGameById(int id)
        {
            var videogame = _repository.GetById(id);

            return videogame;
        }

        public ICollection<VideoGame> ListVideoGames(VideogameRequestParameters query)
        {
            var videogames = _repository.GetAll().ToList();

            if (query?.Search != null)
            {
                var results = videogames.Where(videogame => videogame.Name.ToLower().Contains(query.Search.ToLower())).ToList();

                return results;
            }

            return videogames;
        }

        public void PatchVideoGame(int id, JsonPatchDocument patch)
        {
            var videogame = _repository.GetById(id);

            if (videogame != null)
            {
                _repository.Update(id, patch);
            }

        }

        public MemoryStream DownloadExcel()
        {
            var videogames = _repository.GetAll().ToList();
            var exportVideoGames = videogames.Select(v => new ExcelExportPackage.Models.VideoGame
            {
                Id = v.Id,
                Name = v.Name,
                Developer = v.Developer,
                Description = v.Description,
                ImageUrl = v.ImageUrl,
                Price = v.Price,
                Stock = v.Stock

            }).ToList();

            ExportExcel excelFile = new ExportExcel();
            MemoryStream memoryStream = excelFile.CreateVideogameExcel(exportVideoGames);

            return memoryStream;
        }

        public void UploadExcel()
        {
            var excelFile = new ExportExcel();

            var videogames = excelFile.UploadVideogameExcel().ToList();
            var uploadVideogames = videogames.Select(v => new VideoGame
            {
                Id = v.Id,
                Name = v.Name,
                Developer = v.Developer,
                Description = v.Description,
                ImageUrl = v.ImageUrl,
                Price = v.Price,
                Stock = v.Stock

            }).ToList();

            foreach (VideoGame videogame in uploadVideogames)
            {
                _repository.Add(videogame);
            }
        }
    }
}