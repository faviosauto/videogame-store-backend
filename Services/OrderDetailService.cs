using System.Collections.Generic;
using System.Linq;
using VideoGame_Store_Backend.Services.Interfaces;
using VideoGame_Store_Backend.DataAccess.Repositories.Interfaces;
using VideoGame_Store_Backend.Models;
using VideoGame_Store_Backend.DataAccess.Repositories;
using System.IO;
using ExcelExportPackage;

namespace VideoGame_Store_Backend.Services
{
    public class OrderDetailsService : IOrderDetailService
    {
        readonly IRepository<OrderDetail> _repository;

        public OrderDetailsService(IRepository<OrderDetail> repository)
        {
            _repository = repository;
        }

        public ICollection<OrderDetail> CreateOrderDetail(ICollection<OrderDetail> orderDetail)
        {
            var collectionLength = orderDetail.Count;
            var toArray = orderDetail.ToArray();
            var i = 0;

            while (i <= collectionLength)
            {
                _repository.Add(toArray[i]);
                

                i++;
            }

            return orderDetail;
        }

        public OrderDetail GetOrderDetailById(int id)
        {
            var orderDetail = _repository.GetById(id);

            return orderDetail;
        }

        public ICollection<OrderDetail> ListOrderDetails()
        {
            var orderDetails = _repository.GetAll().ToList();

            return orderDetails;
        }

        public MemoryStream DownloadExcel()
        {
            var orderDetails = _repository.GetAll().ToList();
            var exportOrderDetails = orderDetails.Select(o => new ExcelExportPackage.Models.OrderDetail
            {
                Id = o.Id,
                Quantity = o.Quantity,
                UnitaryPrice = o.UnitaryPrice,
                Subtotal = o.Subtotal

            }).ToList();

            ExportExcel excelFile = new ExportExcel();
            MemoryStream memoryStream = excelFile.CreateOrderExel((ICollection<ExcelExportPackage.Models.Order>)exportOrderDetails);

            return memoryStream;
        }
    }
}