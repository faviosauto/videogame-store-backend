using System;

namespace VideoGame_Store_Backend.Exceptions
{
    public class InventoryMaxedException : Exception
    {
        public InventoryMaxedException()
        { }

        public InventoryMaxedException(string message) : base(message)
        { }

        public InventoryMaxedException(string message, Exception inner) : base(message, inner)
        { }
    }
}