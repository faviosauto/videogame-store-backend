using VideoGame_Store_Backend.DataAccess.Repositories.Interfaces;
using VideoGame_Store_Backend.Models;
using VideoGame_Store_Backend.Services.Interfaces;

namespace VideoGame_Store_Backend.Services
{
    public class UserService : IUserService
    {
        private readonly IRepository<User> _repository;

        public UserService(IRepository<User> repository)
        {
            _repository = repository;
        }

        public User RegisterUser(User user)
        {
            throw new System.NotImplementedException();
        }
    }
}