using System;
using System.Collections.Generic;
using System.IO;
using VideoGame_Store_Backend.Models;

namespace VideoGame_Store_Backend.Services.Interfaces
{
    public interface IOrderService
    {
        ICollection<Order> ListOrders();
        Order GetOrderById(int id);
        Order CreateOrder(Order order);
        public MemoryStream DownloadExcel();
    }
}