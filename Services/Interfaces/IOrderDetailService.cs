using System.Collections.Generic;
using System.IO;
using VideoGame_Store_Backend.Models;

namespace VideoGame_Store_Backend.Services.Interfaces
{
    public interface IOrderDetailService
    {
        ICollection<OrderDetail> ListOrderDetails();
        OrderDetail GetOrderDetailById(int id);
        ICollection<OrderDetail> CreateOrderDetail(ICollection<OrderDetail> orderDetail);
        public MemoryStream DownloadExcel();
    }
}