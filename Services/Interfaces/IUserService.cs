using VideoGame_Store_Backend.Models;

namespace VideoGame_Store_Backend.Services.Interfaces
{
    public interface IUserService
    {
        User RegisterUser(User user);
    }
}