﻿using Microsoft.AspNetCore.JsonPatch;
using System.Collections.Generic;
using System.IO;
using VideoGame_Store_Backend.Models;
using VideoGame_Store_Backend.Models.RequestParameters;

namespace VideoGame_Store_Backend.Services.Interfaces
{
    public interface IVideoGameService
    {
        ICollection<VideoGame> ListVideoGames(VideogameRequestParameters query);

        VideoGame GetVideoGameById(int id);

        VideoGame CreateVideoGame(VideoGame videogame);

        void PatchVideoGame(int id, JsonPatchDocument videoGame);

        void DeleteVideoGame(int id); 

        MemoryStream DownloadExcel();

        void UploadExcel();
    }
}