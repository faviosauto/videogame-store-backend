using ExcelExportPackage;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using VideoGame_Store_Backend.DataAccess.Repositories;
using VideoGame_Store_Backend.DataAccess.Repositories.Interfaces;
using VideoGame_Store_Backend.Exceptions;
using VideoGame_Store_Backend.Models;
using VideoGame_Store_Backend.Services.Interfaces;


namespace VideoGame_Store_Backend.Services
{
    public class OrderService : IOrderService
    {
        private readonly IRepository<Order> _repository;

        public OrderService(IRepository<Order> repository)
        {
            _repository = repository;
        }

        public Order CreateOrder(Order order)
        {
            try
            {
                foreach (var orderDetail in order.OrderDetails)
                {
                    if (orderDetail.Quantity > orderDetail.VideoGame.Stock)
                    {
                        throw new Exception("There are not enough copies of this videogame");
                    }
                }

                order.OrderDate = DateTime.Now;
                _repository.Add(order);

                return order;
            }
            catch (InventoryMaxedException exception)
            {
                throw exception;
            }
        }

        public Order GetOrderById(int id)
        {
            var order = _repository.GetById(id);

            return order;
        }

        public ICollection<Order> ListOrders()
        {
            var orders = _repository.GetAll().ToList();

            return orders;
        }

        public MemoryStream DownloadExcel()
        {
            var orders = _repository.GetAll().ToList();
            var exportOrders = orders.Select(o => new ExcelExportPackage.Models.Order
            {
                Id = o.Id,
                OrderDate = o.OrderDate,
                TotalAmount = o.TotalAmount,
                OrderDetails = (IEnumerable<ExcelExportPackage.Models.OrderDetail>)o.OrderDetails

            }).ToList();

            ExportExcel excelFile = new ExportExcel();
            MemoryStream memoryStream = excelFile.CreateOrderExel(exportOrders);

            return memoryStream;
        }
    }
}